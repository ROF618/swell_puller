#!/usr/bin/python3

# TODO:
"""
    1. write regex to pull the swell height and swell period
    1. surfside:https://www.ndbc.noaa.gov/station_page.php?station=42019
    1. galveston:https://www.ndbc.noaa.gov/station_page.php?station=42035
"""
# Libraries for table_scraper class
from bs4 import BeautifulSoup
import requests
import csv
import re

# Scheduler libs
from apscheduler.schedulers.background import BlockingScheduler
from pytz import utc
import threading

# Rpi libs
import RPi.GPIO as GPIO
from time import sleep, strftime
from datetime import datetime

from luma.core.interface.serial import spi, noop
from luma.core.render import canvas
from luma.core.virtual import viewport
from luma.led_matrix.device import max7219
from luma.core.legacy import text, show_message
from luma.core.legacy.font import proportional, CP437_FONT, LCD_FONT

serial = spi(port=0, device=0, gpio=noop())
device = max7219(serial, width=32, height=8, block_orientation=-90)
device.contrast(5)
virtual = viewport(device, width=32, height=16)

class table_scraper:
    results = []

    def fetch(self, url):
        return requests.get(url)

    def parse(self, html):
        content = BeautifulSoup(html, "lxml")
        # previous table = content.find(id="data")
        table = content.find(id="tbody")
        print(table)
        rows = table.find_all("table",{"bgcolor":"#f0f8fe"})
        #rows = table.find_all("table")
        self.results.append([header.text for header in rows[0].find_all('th')])

        for row in rows:
            self.results.append([data.text for data in row.find_all('td')])
            #print(row)

    def to_csv(self):
        with open('table.csv', 'w') as csv_file:
            writer = csv.writer(csv_file, delimiter=',')
            #print(self.results)
            writer.writerows(self.results)

    def run(self):
        response = self.fetch('https://www.ndbc.noaa.gov/station_page.php?station=42019')
        self.parse(response.text)
        #print(self.results)
        self.to_csv()

class swell_display:

    display_mess = []
    serial = spi(port=0, device=0, gpio=noop())
    device = max7219(serial, width=32, height=8, block_orientation=-90)
    device.contrast(5)
    virtual = viewport(device, width=32, height=16)

    def csv_puller(self):
        #csv_swell = csv.reader(open('table.csv', "r"), delimiter=",")
        with open("table.csv", "r") as f:
            csv_data = csv.reader(f, delimiter=',')
            lines = list(csv_data)
            #print(lines[1])
            #focused_lines = lines[3][12]
            for i in range(2,13):
                self.display_mess.append(lines[3][i])
            print(focused_lines)
            self.display_mess.remove('')

    def display_loop(self):
        while True:
            try:
                final_dis = ['wvht:', self.display_mess[1], 'swh:', self.display_mess[3], 'swp:', self.display_mess[5]]
                show_message(device, str(final_dis), fill="white", font=proportional(LCD_FONT), scroll_delay=0.08)
                self.csv_puller()

            except KeyboardInterrupt:
                GPIO.cleanup()

    def run(self):
        self.csv_puller()
        self.display_loop()
        #print(self.matches)


"""
try:
	while True:
		show_message(device, 'MAKE THE FUCKING SURF PROGRAM', fill="white", font=proportional(LCD_FONT), scroll_delay=0.08)

except KeyboardInterrupt:
	GPIO.cleanup()

"""
sched = BlockingScheduler(daemon=True, timezone=utc)
def test():
    print(10)

if __name__ == '__main__':
    scraper = table_scraper()
    displayer = swell_display()
    scraper.run()
    #sched.add_job(scraper.run, 'interval', minutes=30)
    #sched.add_job(displayer.csv_puller, 'interval', minutes=30)
    #sched.start()
#    displayer.csv_puller()
    displayer.run()
    background = threading.Thread(name='displayer.display_loop', target=displayer.display_loop)
    #background.start()
    #sched.shutdown()



