#!/usr/bin/python3

"""
from luma.core.interface.serial import spi, noop
from luma.core.render import canvas
from luma.led_matrix.device import max7219
from PIL import ImageFont

serial = spi(port=0, device=0, gpio=noop())
device = max7219(serial, width=32, height=8, block_orientation=-90)
msg = "testing the test"

with canvas(device) as draw:
        draw.rectangle(device.bounding_box, outline="white")
        text(draw, (2, 2), "Hello", fill="white", font=proportional(LCD_FONT)

show_message(device, msg, fill="white", font=proportional(CP437_FONT))

Code below is from https://tutorial.cytron.io/2018/11/22/displaying-max7219-dot-matrix-using-raspberry-pi/ and displays text

"""
import RPi.GPIO as GPIO
from time import sleep, strftime
from datetime import datetime

from luma.core.interface.serial import spi, noop
from luma.core.render import canvas
from luma.core.virtual import viewport
from luma.led_matrix.device import max7219
from luma.core.legacy import text, show_message
from luma.core.legacy.font import proportional, CP437_FONT, LCD_FONT

serial = spi(port=0, device=0, gpio=noop())
device = max7219(serial, width=32, height=8, block_orientation=-90)
device.contrast(5)
virtual = viewport(device, width=32, height=16)

try:
	while True:
		show_message(device, 'MAKE THE FUCKING SURF PROGRAM', fill="white", font=proportional(LCD_FONT), scroll_delay=0.08)

except KeyboardInterrupt:
	GPIO.cleanup()

"""
try:
    while True:
        with canvas(virtual) as draw:
            text(draw, (0, 1), "Idris", fill="white", font=proportional(CP437_FONT))
            #text(draw, (0, 1), datetime.now().strftime('%I:%M'), fill="white", font=proportional(CP437_FONT))

except KeyboardInterrupt:
    GPIO.cleanup()
"""
