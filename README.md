# Swell Puller
Swell Puller is small python script that pulls information from NOAA's buoys and displays the swell information on a dot matrix screen. I have this running on a Raspberry Pi 3b+, but I'm sure this will work with a more budget friendly SBC(Raspberry Pi Pico). All you will need is one of the listed SBUs, the dot matrix screen, and an internet connection. Below you will find a shopping list with everything you need to get this up and running.
* The display should be connected via GPIO pins

# Install
- git clone https://gitlab.com/ROF618/swell_puller
- cd swell_puller
- pip install -r requirements.txt
- Set up cron job:
	- set up desired update interval and call swell_puller.py
		* e.g. {interval} /usr/bin/python3 {dir location}/swell_puller.py
	- Set up another cron job a minut or two before swell_puller is called to kill previous instance
		* e.g. {interval} /usr/bin/python3 {dir location}/swell_killer.py

# Need to run {package-manager} install python3-lxml


### Shopping List
1. [Raspberry Pi 3B](https://www.amazon.com/Raspberry-Pi-MS-004-00000024-Model-Board/dp/B01LPLPBS8/ref=sr_1_3?crid=15OMAHJ423PZP&keywords=raspberry+pi+3b&qid=1673919609&sprefix=raspberry+pi+3b%2Caps%2C176&sr=8-3&ufe=app_do%3Aamzn1.fos.f5122f16-c3e8-4386-bf32-63e904010ad0)
1. [Raspberry Pi Pico](https://www.amazon.com/Generic-Raspberry-Pi-Pico-W/dp/B0B72GV3K3/ref=sr_1_3_mod_primary_new?crid=2JVFNOFH0U8X0&keywords=raspberry+pi+pico+wifi&qid=1673919858&sbo=RZvfv%2F%2FHxDF%2BO5021pAnSA%3D%3D&sprefix=raspberry+pi+pico+wifi%2Caps%2C294&sr=8-3)
1. [Led Dot Matrix](https://www.amazon.com/ALAMSCN-MAX7219-Display-Raspberry-Microcontroller/dp/B08KS68GYZ/ref=sr_1_6?keywords=led+dot+matrix+display&qid=1673920523&sprefix=led+dot+matr%2Caps%2C155&sr=8-6)

### Video

![Raspberry Pi 3b+](/VID_20230117_085141109.mp4)
